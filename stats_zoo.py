from scipy.spatial import distance
from scipy import ndimage

import numpy as np

def getVolumeSize(volume):
    '''
    Get volume size in voxels
    :param volume: nd.array
    :return: volume size in voxels
    '''
    # calculate this in voxel level
    volumeSize = np.count_nonzero(volume)

    return volumeSize

def getVolumeSizeMM(volumeSize, voxelSize_axis1, voxelSize_axis2, voxelSize_axis3):
    '''
    Get volume size in mm
    :param volume: nd.array
    :return: volume size in mm
    '''
    # assign voxel size per axis
    firstAxisSize, secondAxisSize, thirdAxisSize = voxelSize_axis1, voxelSize_axis2, voxelSize_axis3
    # declare arrays for calculation of volume size per blob
    volumeSizeMM = volumeSize * (firstAxisSize * secondAxisSize * thirdAxisSize)

    return round(volumeSizeMM, 2)

def findCentroids(volume: list):
    centroidsLst = []

    if len(volume) == 0:
        return [(0, 0, 0)]

    for idxCentroid in range(len(volume)):
        if np.count_nonzero(volume[idxCentroid]) == 0:
            tmp = tuple([0, 0, 0])
        else:
            tmp = tuple([int(itr) for itr in list(ndimage.measurements.center_of_mass(volume[idxCentroid]))])

        centroidsLst.append(tmp)

    return centroidsLst

def getCentroidDifference(centroidVolume_A: list, centroidVolume_B: list):
    '''
    This is a function to calculate how far points from two (non)equal sets are in 1D space.
    It supports multiple centroid calculation.
    :param centroidVolume_A: list of centroids for volume A
    :param centroidVolume_B: list of centroids for volume B
    :return: list with differences between the centroids
    '''

    minCentroidDiff = []
    cnt = 0

    minLen = min(len(centroidVolume_A), len(centroidVolume_B))
    maxLen = max(len(centroidVolume_A), len(centroidVolume_B))

    for itr in range(minLen):
        if minLen == len(centroidVolume_A):
            tmp = [round(distance.euclidean(centroidVolume_A[itr], itr2), 1) for itr2 in centroidVolume_B]
            minCentroidDiff.append((cnt, np.argmin(tmp)))
            # for index found assign highly value to avoid duplicate search in next step
            centroidVolume_B[np.argmin(tmp)] = (999, 999, 999)

        elif minLen == len(centroidVolume_B):
            tmp = [round(distance.euclidean(centroidVolume_B[itr], itr2), 1) for itr2 in centroidVolume_A]
            minCentroidDiff.append((np.argmin(tmp), cnt))
            # for index found assign highly value to avoid duplicate search in next step
            centroidVolume_A[np.argmin(tmp)] = (999, 999, 999)

        cnt += 1

    # fill in rest missing point comparison
    if minLen != maxLen:
        for itr in range(minLen, maxLen):
            if minLen == len(centroidVolume_A):
                minCentroidDiff.append((999, 999))
            else:
                minCentroidDiff.append((999, 999))

    return minCentroidDiff

def clusterVolumes(volume, voxelSize_axis1, voxelSize_axis2, voxelSize_axis3):
    '''
    Find all non-connected volumes from binary volume
    :param volume: nd.array
    :param voxelSize_axis1: voxel size in first axis
    :param voxelSize_axis2: voxel size in second axis
    :param voxelSize_axis3: voxel size in third axis
    :return:
    volumeSize: volume size in voxels per blob
    nr_objects: number of blobs found
    volumeSizeMM: volume size in mm per blob
    maxComponent: blob index with the largest volume
    minComponent: blob index with the smallest volume
    '''

    labeled, nr_objects = ndimage.label(input=volume)

    if nr_objects == 0:
        volumeSize = 0
        volumeSizeMM = 0

    # start indexing without background value - calculate this in voxel level
    elif nr_objects == 1:
        volumeSize = np.zeros(shape=(nr_objects,), dtype=int)
        volumeSizeMM = np.zeros(shape=(nr_objects,), dtype=float)
        volumeSize[0,] = getVolumeSize(labeled[labeled != 0])
        volumeSizeMM[0, ] = getVolumeSizeMM(volumeSize[0, ], voxelSize_axis1, voxelSize_axis2, voxelSize_axis3)

    else:
        # declare arrays for calculation of volume size per blob
        volumeSize = np.zeros(shape=(nr_objects,), dtype=int)
        volumeSizeMM = np.zeros(shape=(nr_objects,), dtype=float)

        for idxSize in range(0, nr_objects):
            volumeSize[idxSize,] = getVolumeSize(labeled[labeled == idxSize + 1])
            volumeSizeMM[idxSize, ] = getVolumeSizeMM(volumeSize[idxSize, ], voxelSize_axis1, voxelSize_axis2, voxelSize_axis3)

    # get blob with minimum non-zero value and maximum value
    if np.count_nonzero(volumeSize) > 0:
        maxComponent = volumeSize.argmax()
        minComponent = volumeSize.argmin()
    else:
        maxComponent = 0
        minComponent = 0

    return labeled, nr_objects, volumeSize, volumeSizeMM, maxComponent, minComponent

def getThresholded(nrLesions, minThresh, vSize, labeled, lesionVolumes):
    thresholdArr = []

    if nrLesions == 0:
        thresholdArr.append(True)

    elif nrLesions == 1:
        if vSize[0,] < minThresh:
            thresholdArr.append(True)
        else:
            thresholdArr.append(False)
        lesionVolumes[:, :, :, 0] = labeled != 0

    else:
        for idxLesion in range(0, nrLesions):
            # if volume smaller than 10 voxels
            if vSize[idxLesion, ] < minThresh:
                thresholdArr.append(True)
            else:
                thresholdArr.append(False)
            lesionVolumes[:, :, :, idxLesion] = labeled == idxLesion

        del idxLesion

    # storing only the one interested
    thresholdedLesionVolume = []
    for idxLesion in range(len(thresholdArr)):
        if not thresholdArr[idxLesion]:
            thresholdedLesionVolume.append(lesionVolumes[:, :, :, idxLesion])
        else:
            pass

    del idxLesion, lesionVolumes, thresholdArr

    return thresholdedLesionVolume