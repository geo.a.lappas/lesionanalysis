# lesionAnalysis

## Description
This is a script for calculating segmentation and classification metrics between two series of volumes.
For example ground_truth and model_predictions. It will return a summary of the calculate metrics in exce format.
It also provides the user with a summary of the required model and GPU memory.

## Installation
Install first the the required libraries using pip in a virtual environment
- python 3.X
- numpy
- pandas
- nibabel
- matplotlib
- sklearn
- scipy
- GeodisTK

## Usage
Adjust the input directories in the function call quantitativeEvaluation.py.

## Support
For issues, please create an issue or contact geo.a.lappas@gmail.com. 

## Contributing
Would you like to contribute? Please contact geo.a.lappas@gmail.com. 

## License
Apache 2.0 license

## Project status
Active.