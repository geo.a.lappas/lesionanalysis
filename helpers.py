import nibabel as nib
import numpy as np

def readNifti2Numpy(dataPath: str):
    '''
    Helper function to read nifti file (sequence or label) and converting it to numpy array
    :param dataPath: path to read nifti file
    :return: numpy nd-array of nifti file and corresponding affine matrix
    '''

    if not isinstance(dataPath, str):
        raise AssertionError('The dataPath should be a string defining the name of the nifti file.')

    niData = nib.load(dataPath)
    affineData = niData.affine
    niData = np.array(niData.get_fdata())

    return niData, affineData