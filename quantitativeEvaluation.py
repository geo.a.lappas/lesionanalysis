from helpers import readNifti2Numpy
from metrics_zoo import *
from stats_zoo import *

import numpy as np
import pandas as pd
import os
import sklearn.metrics as metrics

def lesionLevelQuant(volume1_path: str, volume2_path: str, minThresh: float, fileName:str, absoluteSpace: bool, *args):
    '''
    Function to quantify annotations in patient-level
    :param volume1: file path to read nifti
    :param volume2: file path to read nifti
    :param minThresh: minimum threshold for lesion size
    fileName: name of the file
    :param plotMode: visualization of ROC
    :param absoluteSpace: if calculation will be made on mm or voxel-space
    :return: list of segmentation and classification metrics
    '''

    # loading the volumes
    volume1, _ = readNifti2Numpy(volume1_path)
    volume2, _ = readNifti2Numpy(volume2_path)

    if len(args) > 0:
        if isinstance(args, tuple):
            voxelSize_axis1, voxelSize_axis2, voxelSize_axis3 = args[0], args[1], args[2]

    else: # assuming isotropic 1 cubic mm voxel
        voxelSize_axis1 = 1
        voxelSize_axis2 = 1
        voxelSize_axis3 = 1

    # cluster the volumes and calculate the size
    labeled_A, nrLesionsA, vSize_A, vSizeMM_A, maxBlob_A, minBlob_A = clusterVolumes(volume1, voxelSize_axis1, voxelSize_axis2, voxelSize_axis3)
    labeled_B, nrLesionsB, vSize_B, vSizeMM_B, maxBlob_B, minBlob_B = clusterVolumes(volume2, voxelSize_axis1, voxelSize_axis2, voxelSize_axis3)

    # declaring the required arrays for volumes A and B
    lesionVolumes_A = np.zeros(shape=labeled_A.shape + (nrLesionsA, ), dtype=bool)
    lesionVolumes_B = np.zeros(shape=labeled_B.shape + (nrLesionsB, ), dtype=bool)

    # threshold based on min value the volumes 1 and 2
    thresholdedLesionVolume_A = getThresholded(nrLesionsA, minThresh, vSize_A, labeled_A, lesionVolumes_A)
    thresholdedLesionVolume_B = getThresholded(nrLesionsB, minThresh, vSize_B, labeled_B, lesionVolumes_B)

    # declare the various arrays
    DSC = []
    HD95 = []
    MSD = []
    sens_score = []
    spec_score = []
    fpr = []
    tpr = []
    roc_auc = []
    lesionNbr = []
    fileNameLst = []

    # Calculate centroids
    centroidsA = findCentroids(volume=thresholdedLesionVolume_A)
    centroidsB = findCentroids(volume=thresholdedLesionVolume_B)

    # Calculate centroid differences
    diffCentroids = getCentroidDifference(centroidVolume_A=centroidsA, centroidVolume_B=centroidsB)

    # Now make the comparisons by indexing the two volumes
    for idxLesion in range(len(diffCentroids)):
        idxA = diffCentroids[idxLesion][0]
        idxB = diffCentroids[idxLesion][1]

        if idxA == 999 or idxB == 999 or nrLesionsA == 0 or nrLesionsB == 0:
            DSC.append(float(0))
            HD95.append(float('inf'))
            MSD.append(float('inf'))

            sens_score.append(0)  # this is tpr
            spec_score.append(0)  # this is 1-fpr

            # Calculate area under the curve
            fprTemp, tprTemp, thresholdTemp = float('inf'), float('inf'), float('inf')
            fpr.append(fprTemp)
            tpr.append(tprTemp)
            roc_auc.append(0)

        else:
            DSC.append(round(DiceCoeff(thresholdedLesionVolume_A[idxA], thresholdedLesionVolume_B[idxB]), 3))
            HD95.append(round(HausdorffDistance95(thresholdedLesionVolume_A[idxA], thresholdedLesionVolume_B[idxB]), 3))
            MSD.append(round(MeanSurfaceDistane(thresholdedLesionVolume_A[idxA], thresholdedLesionVolume_B[idxB]), 3))

            # calculation classification metrics
            vectorA_largest = vectorizeArray(thresholdedLesionVolume_A[idxA])
            vectorB_largest = vectorizeArray(thresholdedLesionVolume_B[idxB])

            # Calculate sensitivity and specificity
            sens_score.append(round(sensitivity(vectorA_largest, vectorB_largest), 3)) # this is tpr
            spec_score.append(round(specificity(vectorA_largest, vectorB_largest), 3)) # this is 1-fpr
            # Calculate area under the curve
            fprTemp, tprTemp, thresholdTemp = metrics.roc_curve(vectorA_largest, vectorB_largest)
            fpr.append(fprTemp)
            tpr.append(tprTemp)
            roc_auc.append(round(metrics.auc(fprTemp, tprTemp), 3))

        lesionNbr.append(idxLesion)
        fileNameLst.append(fileName)

    # Make a new list with the headers and metrics
    data = []

    maxLesionCnt = max(nrLesionsA, nrLesionsB)

    if maxLesionCnt == nrLesionsA:
        vSizeGlob = vSize_A
    else:
        vSizeGlob = vSize_B

    for idxLesion in range(len(diffCentroids)):
        tmpLst = [fileNameLst[idxLesion], lesionNbr[idxLesion], DSC[idxLesion], HD95[idxLesion], MSD[idxLesion], sens_score[idxLesion], spec_score[idxLesion], roc_auc[idxLesion]]
        data.append(tmpLst)

    return data, fileName

def wrapAnalysis(dirMask_1, dirMask_2):
    '''
    This function is a wrapper for the lesionLevelQuant function.
    It will calculate the metrics for each lesion and save them into an excel file.
    :param dirMask_1: directory of the first mask
    :param dirMask_2: directory of the second mask

    :return: excel file with the metrics
    '''

    list_1 = sorted(os.listdir(dirMask_1))
    list_2 = sorted(os.listdir(dirMask_2))

    list_common = sorted(set(list_1).intersection(list_2))

    sumMetricsOverview = []

    for itrTemp in range(len(list_common)):
        mask1 = dirMask_1 + list_common[itrTemp]
        mask2 = dirMask_2 + list_2[itrTemp]
        curFileName = os.path.basename(list_common[itrTemp]).split('.nii.gz')[0]

        outLst, _ = lesionLevelQuant(volume1_path=mask1, volume2_path=mask2, minThresh=1, fileName=curFileName,
                                     absoluteSpace=False)

        print('Metrics have been calculated for case', curFileName)

        sumMetricsOverview.append(outLst)

    # Merge everything into a single list
    headerLst = ["Case", "LesionNumber", "DSC", "HD95", "MSD", "Sensitivity", "Specificity", "AreaUnderCurve"]

    # Flatten list
    flat_sumMetricsOverview = []

    for xs in sumMetricsOverview:
        for x in xs:
            flat_sumMetricsOverview.append(x)

    # And write it into an excel file
    df = pd.DataFrame(flat_sumMetricsOverview, columns=headerLst)
    df.to_excel(os.path.join(os.path.dirname(dirMask_1), 'metricsOverview.xlsx'), index=False)

    print('Analysis is complete and the metrics have been calculated.')

if __name__ == '__main__':
    dirMask_gt = '~/gt_segmentations/'
    dirMask_pred = '~/fold_0/validation/'

    wrapAnalysis(dirMask_1=dirMask_gt, dirMask_2=dirMask_pred)
